package edu.codecnetworks.guidemo;


public class Dogs {

	private String name;
	private int size;
	 private int noOfTeeth;

	public void setSize(int val) throws Exception
	{
		if (val > 0 && val <= 100)
		size = val;
		else
        throw new Exception("Error ! Dog size should be between 0 and 100. ");
	}

	public int getSize() {   return size;  }

	void bite(String name) {

		System.out.println(name + " cries OUCHHH !");

	}
	


	void bark(int times) {

		for (int i = 0; i < times; i++) {
			if (size >= 70 && size <= 100)
				System.out.println("Bhoww !");

			else if (size >= 40 && size < 70)
				System.out.println("Ruff");

			else if (size > 0 && size < 40)
				System.out.println("Wooff ");

			else
				System.out.println("hawwff ");

		}

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNoOfTeeth() {
		return noOfTeeth;
	}

	public void setNoOfTeeth(int noOfTeeth) {
		this.noOfTeeth = noOfTeeth;
	}
}
