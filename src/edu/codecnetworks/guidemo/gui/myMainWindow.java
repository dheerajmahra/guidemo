package edu.codecnetworks.guidemo.gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import edu.codecnetworks.guidemo.Dogs;

public class myMainWindow {

	JFrame frame = new JFrame("GUI Application");
	JPanel[] panel=new JPanel[4];
	JLabel[] label=new JLabel[3];
	JTextField[] textField=new JTextField[3];
	JButton submitButton=new JButton("SUBMIT");
	
	Dogs dog=new Dogs();

	public myMainWindow() {
		
		submitButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				dog.setName(textField[0].getText());
				try {
					dog.setSize(Integer.parseInt(textField[1].getText()));
				} catch (Exception e){
					textField[1].setForeground(Color.RED);
					textField[1].setText(e.getMessage());
				}
				
				try{
					dog.setNoOfTeeth(Integer.parseInt(textField[2].getText()));
				}
				
				catch(Exception e){
					textField[2].setForeground(Color.RED);
					textField[2].setText(e.getMessage());
					
				}
				
				
			}
			
		});
		
        
		for(int i=0;i<label.length;i++)
		{  
			label[i]=new JLabel();
			textField[i]=new JTextField(10);
		}
		
		label[0].setText("Enter dog's name: ");
		label[1].setText("Enter dog's height: ");
		label[2].setText("Enter no. of teeth: ");
		
		for(int i=0;i<panel.length;i++){
			
			panel[i]=new JPanel();
			panel[i].setLayout(new BoxLayout(panel[i],BoxLayout.X_AXIS));
		}
		
		for(int i=0;i<label.length;i++)
		{
		panel[i].add(label[i]);
		panel[i].add(textField[i]);
		}
		
		panel[3].setLayout(new BoxLayout(panel[3],BoxLayout.Y_AXIS));
		panel[3].add(panel[0]);
		panel[3].add(panel[1]);
		panel[3].add(panel[2]);
		panel[3].add(submitButton);
		
		frame.getContentPane().add(panel[3]);
		frame.setVisible(true);
		frame.setSize(400, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}